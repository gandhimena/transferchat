//
//  TutorialViewController.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 21/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit
import WebKit

class TutorialViewController: UIView{
    
    var webView :WKWebView = {
       let webView = WKWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    var closeButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.setImage(UIImage(named: "close"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        //webView.navigationDelegate = self
        webView = WKWebView()
        let urlString  = "https://www.apple.com/"
        let url = URL(string: urlString)
       
        
        
        let urlRequest = URLRequest(url: url!)
        webView.load(urlRequest)
        self.addSubview(webView)
        self.addSubview(closeButton)
        
//        webView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//        webView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
//        webView.leftAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
//        webView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive
        addConstrainWithFormat(format: "H:|[v0]|", views: webView)
        addConstrainWithFormat(format: "V:|[v0]|", views: webView)
        
        closeButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}




//extension TutorialViewController: WKNavigationDelegate{
//
//}
