//
//  Friend.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 10/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation


struct Friend {
    var name: String?
    var profileImageName: String?
    var messages:[Message]?
}
