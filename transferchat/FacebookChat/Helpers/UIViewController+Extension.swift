//
//  UIViewController+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 17/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

enum Result<T,E>{
    case success(_ :T)
    case failure(_ :E)
}


extension UIViewController{
    func runOnTheDeviceType(_ completion: (DeviceTypeModel) -> Void) {
        if UIDevice().userInterfaceIdiom == .phone{
            switch UIScreen.main.nativeBounds.height{
            case 2436:
                completion(.iphoneX)
            case 1920:
                completion(.iphone8Plus)
            case 1334:
                completion(.iphone8)
            case 1136:
                completion(.iphoneSE)
            default:
                completion(.iphone4s)
            }
        }
    }
}
