//
//  ChatViewController.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 14/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

class ChatViewController: UICollectionViewController{

    //backgorundInputView
    var backgroundInputView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .white
        return view
    }()
    
    //MessageInputView
    var messageInputContainerView: MessageInputContainerView = {
       let view = MessageInputContainerView()
        view.backgroundColor = .white
        view.borders(for: [.top], width: 1, color: UIColor.grayMedium)
        return view
    }()
    var constraintBottomMessageInputView: NSLayoutConstraint!
    var constraintBottomBackgroundInput: NSLayoutConstraint!
    
    
    //cell
    let cellid = "cellID"
    
    //Messages
    var messages:[Message]?
    
    var friend: Friend?{
        didSet{
            navigationItem.title = friend?.name
        }
    }
    
    //Tandas
    var tandas:[Tanda] = []
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            DispatchQueue.main.async {
                self.collectionView?.scrollToItem(at: IndexPath(row: self.messages!.count - 1, section: 0), at: .bottom, animated: true)
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        //Keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        //CollectionView
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatCell.self, forCellWithReuseIdentifier: cellid)
        
        //backgroundInputView
        view.addSubview(backgroundInputView)
        view.addConstrainWithFormat(format: "H:|[v0]|", views: backgroundInputView)
        view.addConstrainWithFormat(format: "V:[v0(79)]", views: backgroundInputView)
        constraintBottomBackgroundInput = NSLayoutConstraint(item: backgroundInputView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(constraintBottomBackgroundInput)
        
        //textInputView
        view.addSubview(messageInputContainerView)
        view.addConstrainWithFormat(format: "H:|[v0]|", views: messageInputContainerView)
        view.addConstrainWithFormat(format: "V:[v0(45)]", views: messageInputContainerView)
        runOnTheDeviceType { devices in
            switch devices{
            case .iphoneX:
                backgroundInputView.isHidden = false
                constraintBottomMessageInputView = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -34)
            default:
                backgroundInputView.isHidden = true
                constraintBottomMessageInputView = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            }
        }
        view.addConstraint(constraintBottomMessageInputView)
    }
    
    //////////////////////////////
    //MARK: - Keyboard events
    /////////////////////////////
    @objc func keyboardWillShow(notification: NSNotification){
        if let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
            
            let isKeyboardShowing = notification.name == Notification.Name.UIKeyboardWillShow
            constraintBottomMessageInputView.constant = isKeyboardShowing ? -keyboardFrame.height : 0
            
            UIView.animate(withDuration: 0, animations: {
                self.view.layoutIfNeeded()
            }) { completed in
                self.runOnTheDeviceType { device in
                    switch device{
                    case .iphoneX:
                        self.constraintBottomMessageInputView.constant = -keyboardFrame.height
                        self.constraintBottomBackgroundInput.constant = -keyboardFrame.height + 34
                        
                    default:
                        self.constraintBottomMessageInputView.constant = -keyboardFrame.height
                        self.constraintBottomBackgroundInput.constant = -keyboardFrame.height
                    }
                }
                
                guard let messages = self.messages else{return}
                if !messages.isEmpty {
                    DispatchQueue.main.async {
                        self.collectionView?.scrollToItem(at: IndexPath(row: self.messages!.count - 1, section: 0), at: .bottom, animated: true)
                    }
                }
            }
        }
        
    }
    @objc func keyboardWillHide(notification: NSNotification){
        runOnTheDeviceType { devices in
            switch devices{
            case .iphoneX:
                self.constraintBottomMessageInputView.constant = -34
                self.constraintBottomBackgroundInput.constant = 0
            default:
                self.constraintBottomBackgroundInput.constant = 0
                self.constraintBottomMessageInputView.constant = 0
            }
        }
        self.view.layoutIfNeeded()
    }
    
    
    func loadMessages(){
        
    }
    
    func createMessage(text: String, userName: String, profileImageName: String, isSender: Bool = false) -> Message{
        var message = Message()
        message.text = text
        message.friend?.profileImageName = profileImageName
        message.isSender = isSender
        return message
    }
    
}






