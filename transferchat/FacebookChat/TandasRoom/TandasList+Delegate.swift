//
//  TandasList+Delegate.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 18/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit


extension FriendsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: 80)
        default:
            return CGSize(width: view.frame.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            return UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        }
    }
}
