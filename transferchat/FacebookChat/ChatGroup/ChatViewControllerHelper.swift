//
//  ChatViewControllerHelper.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 14/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

extension ChatViewController{
    func setupData(){
        
        let message1 = createMessage(text: "Hello, my name is Gandhi let's me know what do you want to kno iuashi viduvh iodfah vioh io hiova oivhaoiv iohvioh voiaoivh aiohvioah ioah oihoih aioehioagoiaoigoihoia ioaiogha iogioag oiagih eoiagiahioga iohg oiag oiagoia gw", userName: "Gandhi", profileImageName: "gandhi")
        
        let message2 = createMessage(text: "Hello, my name is Gandhi let's me know what do you want tokuh qiufgeqiugfiuqegfui gqewfiug iufg know", userName: "Christ", profileImageName: "gandhi")
        
        let message3 = createMessage(text: "Hello, my name is Gandhi let's me know", userName: "Bryan", profileImageName: "gandhi")
        
        let message4 = createMessage(text: "Hello, my name is Gandhi let's me know what do you want to kno iuashi viduvh iodfah vioh io hiova oivhaoiv iohvioh voiaoivh aiohvioah ioah oihoih aioehioagoiaoigoihoia ioaiogha iogioag oiagih eoiagiahioga iohg oiag oiagoia gw", userName: "Bryan", profileImageName: "gandhi", isSender: true)
        
        let message5 = createMessage(text: "Hello, my name is Gandhi let's me know what do you want tokuh qiufgeqiugfiuqegfui gqewfiug iufg know", userName: "Christ", profileImageName: "gandhi", isSender: true)
        
        let message6 = createMessage(text: "Hello, my name is Gandhi let's me know", userName: "Bryan", profileImageName: "gandhi", isSender: true)
        
        messages = [message1,message2, message3, message4, message5, message6]
        
    }
}
