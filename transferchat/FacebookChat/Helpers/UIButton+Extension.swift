//
//  UIButton+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 18/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

extension UIButton{

    func standardWithShadow(BackgroundColor: UIColor? = nil, borderColor: UIColor? = nil, borderWidth: Int? = nil){
        self.layer.cornerRadius = 5
        //self.layer.masksToBounds = false
        self.layer.borderWidth = CGFloat(borderWidth ?? 0)
        self.layer.borderColor = borderColor?.cgColor ?? UIColor.clear.cgColor
        self.backgroundColor = BackgroundColor ?? UIColor.pink
        self.tintColor = .white
        
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
    }
}
