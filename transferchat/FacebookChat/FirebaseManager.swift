//
//  FirebaseManager.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 11/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation
import Firebase

class FirebaseManager {

    var channelRefHandle:DatabaseHandle?
    
    private func createChannel(name:String, reference:DatabaseReference){
        let newTandaRef = reference.childByAutoId() //Crea una nueva referencia con un id
        let tandaItem = ["name": name] // Este será el nombre de la nueva referencia(canal)
        newTandaRef.setValue(tandaItem) //Le añadimos a la nueva referencia el nuevo nombre que tendrá el canal
    }
    
}
