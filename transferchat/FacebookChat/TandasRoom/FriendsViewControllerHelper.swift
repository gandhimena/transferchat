//
//  FriendsViewControllerHelper.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 11/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation

extension FriendsViewController{
    func setupData(){
        
        var gandhi = Friend()
        gandhi.name = "Gandhi"
        gandhi.profileImageName = "gandhi"
        
        var messageGandhi = Message()
        messageGandhi.date = Date()
        messageGandhi.text = "Hello, my name is Gandhi let's me know what do you want to know"
        messageGandhi.friend = gandhi
        
        var christ = Friend()
        christ.name = "Christ"
        christ.profileImageName = "christ"
        
        var messagePedro = Message()
        messagePedro.date = Date()
        messagePedro.text = "Hello, my name is Gandhi let's me know what do you want to know"
        messagePedro.friend = christ
        
        messages = [messageGandhi, messagePedro]
    }
    func setupButtons(){
        buttons = ["Nueva tanda", "Ver tutorial"]
    }
}
