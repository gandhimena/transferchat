//
//  ChatCell.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 14/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

enum BubbleMessageType{
    case left
    case right
}

extension BubbleMessageType{
    var image: UIImage {
        switch self{
        case .left:
            return UIImage(named:"bubbleMessage_left")!.resizableImage(withCapInsets: UIEdgeInsets(top: 32, left: 32, bottom: 32, right: 32)).withRenderingMode(.alwaysTemplate)
        case .right:
            return UIImage(named:"bubbleMessage_right")!.resizableImage(withCapInsets: UIEdgeInsets(top: 32, left: 32, bottom: 32, right: 32)).withRenderingMode(.alwaysTemplate)
        }
    }
}


class ChatCell: BaseCell{
    
    let messageText: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.text = "Esto es un aprueba de texto"
        textView.isEditable = false
        textView.backgroundColor = .clear
        return textView
    }()
    
    let bubbleView: UIView = {
        let view = UIView()
        //view.layer.cornerRadius = 10
        view.clipsToBounds = true
        //view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        return view
    }()
    
    let bubbleImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = BubbleMessageType.left.image
        imageView.tintColor = UIColor(white: 0.90, alpha: 1)
        return imageView
    }()
    
    let imageProfile: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = .clear
        
        addSubview(bubbleView)
        addSubview(messageText)
        addSubview(imageProfile)
        bubbleView.addSubview(bubbleImage)
        
        //ImageProfileConstraints
        addConstrainWithFormat(format: "H:|-10-[v0(30)]", views: imageProfile)
        addConstrainWithFormat(format: "V:[v0(30)]|", views: imageProfile)
        imageProfile.backgroundColor = .red
        
        //bubbleImageConstraint
        bubbleView.addConstrainWithFormat(format: "H:|[v0]|", views: bubbleImage)
        bubbleView.addConstrainWithFormat(format: "V:|[v0]|", views: bubbleImage)
    }
}
