//
//  Message.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 10/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation


struct Message{
    var text: String?
    var date: Date?
    var friend: Friend?
    var isSender: Bool?
    
    init(text: String? = nil, date: Date? = nil, friend: Friend? = nil, isSender: Bool? = nil) {
        self.text = text
        self.date = date
        self.friend = friend
        self.isSender = isSender
    }
}
