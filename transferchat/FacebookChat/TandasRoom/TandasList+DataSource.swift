//
//  TandasList+DataSource.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 18/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit
import Foundation

extension FriendsViewController{ //UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return tandas.count
            
        default: //Buttons
            return 2
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0: //tandas List
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: messageCellId, for: indexPath) as! MessageCell
            print(tandas.count)
            if tandas.count == 0 {return cell}
            //guard let title = tandas[indexPath.row].name else{return cell}
            cell.nameLabel.text = tandas[indexPath.row].name
            //cell.message = message
            
            return cell
        default: //buttons
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: buttonCellId, for: indexPath) as! ButtonCell
            
            let buttonTitle = buttons[indexPath.item]
            switch buttonTitle{
            case "Nueva tanda":
                let tap = UITapGestureRecognizer(target: self, action: #selector(createNewTanda))
                cell.button.addGestureRecognizer(tap)
                cell.button.standardWithShadow()
                cell.button.setTitle(buttonTitle.uppercased(), for: .normal)
            case "Ver tutorial":
                let tap = UITapGestureRecognizer(target: self, action: #selector(watchTutorial))
                cell.button.addGestureRecognizer(tap)
                cell.button.standardWithShadow(BackgroundColor: .white, borderColor: .bluelight, borderWidth: 2)
                cell.button.setTitleColor(UIColor.bluelight, for: .normal)
                cell.button.setTitle(buttonTitle.uppercased(), for: .normal)
            default:()
            }
            
            //cell.backgroundColor = .gray
            return cell
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            let layout = UICollectionViewFlowLayout()
            let collectionView = ChatViewController.init(collectionViewLayout:layout )
            navigationController?.pushViewController(collectionView, animated: true)
        default:
            let title = buttons[indexPath.row]
            print(title)
        }
        
    }
    
}
