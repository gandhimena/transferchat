//
//  MessageInputContainerView.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 16/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

class MessageInputContainerView: UIView{
    
    let inputTextField: UITextField = {
        let textField = UITextField()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 19, height: 39))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
        textField.layer.borderColor = UIColor.graySoft.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 17
        textField.placeholder = "Enter message ..."
        return textField
    }()
    
    let sendButton: UIButton = {
        let button = UIButton()
        //button.setTitle("Send", for: .normal)
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named:"send_button"), for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(inputTextField)
        addSubview(sendButton)
        addConstrainWithFormat(format: "H:|-10-[v0]-5-[v1(30)]-10-|", views: inputTextField,sendButton)
        addConstrainWithFormat(format: "V:|-5-[v0(35)]", views: inputTextField)
        addConstrainWithFormat(format: "V:|-8-[v0(30)]", views: sendButton)
        
//        inputTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
//        inputTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
//        inputTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//        inputTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
