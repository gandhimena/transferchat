//
//  buttonsCell.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 18/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

class ButtonCell: BaseCell{
    
    let button: UIButton = {
        let button = UIButton()
        button.standardWithShadow()
        button.isUserInteractionEnabled = true
        return button
    }()
    
    override func setupViews() {
        
        addSubview(button)
        
        addConstrainWithFormat(format: "H:|-45-[v0]-45-|", views: button)
        addConstrainWithFormat(format: "V:|[v0(46)]|", views: button)
        
    }
    
    
}
