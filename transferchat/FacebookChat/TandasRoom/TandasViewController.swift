//
//  ViewController.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 04/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit
import Firebase

class FriendsViewController: UICollectionViewController{
    
    //cell
    let messageCellId = "messageCellId"
    let buttonCellId = "buttonCellId"
    
    //Messages
    var messages:[Message]?
    
    //buttonsTandas
    var buttons:[String] = []
    
    //Tandas
    var tandas:[Tanda] = []
    
    var constraintHeightTutorial: NSLayoutConstraint!
    
    lazy var tutorial: TutorialViewController = {
        let view = TutorialViewController()
        view.backgroundColor = .gray
        view.frame = CGRect(x: 0, y: self.view.frame.height, width: 0, height: 0)
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.isHidden = true
        return view
    }()
    
    
    //Firebase
    lazy var channelRef = Database.database().reference().child("tandas") //referencia de la lista de canales de la base de datos de Firebase
    var channelRefHandle:DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signInFirebaseAuth()
        observeChannels()
        setupData()
        setupButtons()
        
        
        //tutorial.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height).isActive = true
        //tutorial.heightAnchor.constraint(equalToConstant: 0).isActive = true
        
        tutorial.closeButton.addTarget(self, action: #selector(closeTutorial), for: .touchUpInside)
        //constraintHeightTutorial = NSLayoutConstraint(item: tutorial, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: self.view.frame.height)
        
        title = "Tandas Transfer"
        
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(MessageCell.self, forCellWithReuseIdentifier: messageCellId)
        collectionView?.register(ButtonCell.self, forCellWithReuseIdentifier: buttonCellId)
        
        
        //view.addConstraint(constraintHeightTutorial)
        
    }
    
    deinit {
        //Firebase
        if let refhandle = channelRefHandle{
            channelRef.removeObserver(withHandle: refhandle)
        }
    }
    
    @objc func createNewTanda(){
        print("New Tanda")
    }
    
    @objc func watchTutorial(){
        print("Watch tutorial")
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            
            
            
            
            
            //Tutorial
            self.view.addSubview(self.tutorial)
            self.tutorial.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.tutorial.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            self.tutorial.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            self.tutorial.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
            self.tutorial.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 40 * (UIScreen.main.bounds.height / 100)).isActive  = true
            
            
        }
    }
    
    @objc func closeTutorial(){
        print("closeTutorial")
        UIView.animate(withDuration: 0.5, animations: {
            self.tutorial.frame = CGRect(x: 0, y: self.view.frame.height, width: 0, height: 0)
            self.tutorial.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 70 * (UIScreen.main.bounds.height / 100)).isActive  = true
        }) { (true) in
            self.tutorial.removeFromSuperview()
        }
        
    }
    
}

extension FriendsViewController{ //Firebase
    
    //Auth Anonymous
    func signInFirebaseAuth(){
        //Anonymous SignIn
        Auth.auth().signInAnonymously { (user, error) in
            if let err = error{ return print(err.localizedDescription)}
        }
    }
    
    //Obtener lista de canales para mi TableView
    func observeChannels(){
        channelRefHandle = channelRef.observe(.childAdded , with: { (snapshot) -> Void in //observa que se añadío una nueva tanda a la lista
            let tandaData = snapshot.value as! Dictionary<String, AnyObject>
            let id = snapshot.key
            if let name = tandaData["name"] as! String?, name.count > 0{
                self.tandas.append(Tanda(id: id, name: name))
                self.collectionView?.reloadData()
            }else{
                print("Error! Could not decode channel data")
            }
        })
    }
    
    //Create
    func createTanda(name: String){
        let newTandaRef = channelRef.childByAutoId() //Crear una nueva referencia con un id
        let tandaItem = ["name": name] // Este será el nombre de la nueva referencia(canal)
        newTandaRef.setValue(tandaItem) //Le añadimos a la nueva referencia el nuevo nombre que tendrá el canal
        collectionView?.reloadData()
    }
    
}





