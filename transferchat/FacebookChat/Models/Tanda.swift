//
//  Tanda.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 11/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation

internal class Tanda {
    internal let id: String?
    internal let name: String?
    
    init(id: String? = nil, name:String? = nil) {
        self.id = id
        self.name = name
    }
}

