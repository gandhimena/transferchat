//
//  UIColor+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 17/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

extension UIColor{
    @nonobjc class var graySoft: UIColor {
        return UIColor(red: 217/255, green: 218/255, blue: 221/255, alpha: 1)
    }
    @nonobjc class var grayMedium: UIColor {
        return UIColor(red: 204/255, green: 205/255, blue: 209/255, alpha: 1)
    }
    @nonobjc class var pink: UIColor {
        return UIColor(red: 218/255, green: 57/255, blue: 138/255, alpha: 1)
    }
    
    @nonobjc class var bluelight: UIColor {
        return UIColor(red: 80/255, green: 176/255, blue: 247/255, alpha: 1)
    }
    
}
