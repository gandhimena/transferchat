//
//  UIView+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 07/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

enum DeviceTypeModel{
    case iphoneX
    case iphone8Plus
    case iphone8
    case iphoneSE
    case iphone4s
}

extension UIView{
    func addConstrainWithFormat(format:String, views: UIView...){
        
        var viewsDictionary = [String:UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        let newformat = format.removingWhitespaces()
        print(newformat)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: newformat, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func runOnTheDeviceType(_ completion: (DeviceTypeModel) -> Void) {
        if UIDevice().userInterfaceIdiom == .phone{
            switch UIScreen.main.nativeBounds.height{
            case 2436:
                completion(.iphoneX)
            case 1920:
                completion(.iphone8Plus)
            case 1334:
                completion(.iphone8)
            case 1136:
                completion(.iphoneSE)
            default:
                completion(.iphone4s)
            }
        }
    }
    
    @discardableResult
    func addAutolayoutSubview(_ view: UIView, matchingAttributes: [NSLayoutAttribute]? = nil) -> [NSLayoutConstraint] {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        if let matchingAttributes = matchingAttributes {
            let constraints = matchingAttributes.map {
                NSLayoutConstraint(item: view, attribute: $0, relatedBy: .equal, toItem: self, attribute: $0, multiplier: 1, constant: 0)
            }
            addConstraints(constraints)
            return constraints
        } else {
            return []
        }
    }
    
    @discardableResult
    func addFillingSubview(_ view: UIView) -> [NSLayoutConstraint] {
        return addAutolayoutSubview(view, matchingAttributes: [.leading, .trailing, .top, .bottom])
    }
}
