//
//  ChatViewController+DataSource.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 16/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

extension ChatViewController{ //UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = messages?.count else{ return 0}
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! ChatCell
        if let message = messages?[indexPath.item], let messageText = message.text{
            cell.messageText.text = message.text
            cell.imageProfile.image = UIImage(named:message.friend?.profileImageName ?? "gandhi")
            
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)], context: nil)
            switch message.isSender!{
            case true:
                cell.messageText.frame = CGRect(x: view.frame.width - estimatedFrame.width - 10, y: 0, width: estimatedFrame.width - 12, height: estimatedFrame.height + 20)
                cell.bubbleView.frame = CGRect(x:view.frame.width - estimatedFrame.width - 15, y: 0, width: estimatedFrame.width, height: estimatedFrame.height + 20)
                cell.imageProfile.isHidden = true
                cell.messageText.textColor = .white
                cell.bubbleImage.tintColor = .blue
                cell.bubbleImage.image = BubbleMessageType.right.image
            case false:
                cell.messageText.frame = CGRect(x: 65, y: 0, width: estimatedFrame.width, height: estimatedFrame.height + 20)
                cell.bubbleView.frame = CGRect(x: 50, y: 0, width: estimatedFrame.width + 15 , height: estimatedFrame.height + 20)
                cell.imageProfile.isHidden = false
                cell.messageText.textColor = .black
                cell.bubbleImage.image = BubbleMessageType.left.image
            }
        }
        return cell
    }
    
}
