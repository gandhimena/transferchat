//
//  TandasController.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 18/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation
import Firebase

protocol FirebaseControllerAssebler {
    func resolve() -> TandasController
}

extension FirebaseControllerAssebler{
    func resolve() -> TandasController{
        return TandasController()
    }
}

class TandasController{
    
    var channelRefHandle:DatabaseHandle?
    
    //Auth Anonymous
    func signInFirebaseAuth(){
        //Anonymous SignIn
        Auth.auth().signInAnonymously { (user, error) in
            if let err = error{ return print(err.localizedDescription)}
            //self.performSegue(withIdentifier: "LoginToChat", sender: nil)
        }
    }
    
    //Obtener lista de canales para mi TableView
    func observeChannels(dbReference: DatabaseReference, collectionView: UICollectionView) -> [Tanda]{
        var tandas: [Tanda] = []
        channelRefHandle = dbReference.observe(.childAdded , with: { (snapshot) -> Void in //observa que se añadío una nueva tanda a la lista
            let tandaData = snapshot.value as! Dictionary<String, AnyObject>
            let id = snapshot.key
            if let name = tandaData["name"] as! String?, name.count > 0{
                tandas.append(Tanda(id: id, name: name))
                collectionView.reloadData()
            }else{
                print("Error! Could not decode channel data")
            }
        })
        return tandas
    }
    
    //Create
    func createTanda(dbReference:DatabaseReference ,name: String){
        let newTandaRef = dbReference.childByAutoId() //Crear una nueva referencia con un id
        let tandaItem = ["name": name] // Este será el nombre de la nueva referencia(canal)
        newTandaRef.setValue(tandaItem) //Le añadimos a la nueva referencia el nuevo nombre que tendrá el canal
    }
    
}
