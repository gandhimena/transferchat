//
//  Date+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 10/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation
import UIKit

enum DateFormatterStyle{
    case df_hour
    case df_minutes
    case df_seconds
    case df_h_m_s
    case df_h_m_am_pm
    case df_day
    case df_dd
    case df_dd_mm
    case df_dd_month
    case df_dd_mm_yyyy
    case df_dd_mm_yyyy_h_mm_a
    case df_dd_mm_yyyy_24h
}

extension DateFormatterStyle{
    var style: String {
        switch self {
        case .df_hour:
            return "hh"
        case .df_minutes:
            return "mm"
        case .df_seconds:
            return "ss"
        case .df_h_m_s:
            return "HH:mm:ss"
        case .df_h_m_am_pm:
            return "h:mm a"
        case .df_day:
            return "EEEE"
        case .df_dd:
            return "dd"
        case .df_dd_mm:
            return ""
        case .df_dd_month:
            return "MMM d"
        case .df_dd_mm_yyyy:
            return "MM/dd/yyyy"
        case .df_dd_mm_yyyy_h_mm_a:
            return "MM/dd/yyyy, h:mm a"
        case .df_dd_mm_yyyy_24h:
            return "MM/dd/yyyy, h:mm"
        }
    }
}

extension Date{
    func formatterToString(format:DateFormatterStyle, style: DateFormatter.Style? = nil,timeZone: TimeZone? = nil ) -> String{
        let formatter = DateFormatter()
        formatter.dateStyle =  style ?? .medium
        formatter.timeZone = timeZone ?? TimeZone.current
        formatter.dateFormat = format.style
        return formatter.string(from: self)
    }
 
    
}
