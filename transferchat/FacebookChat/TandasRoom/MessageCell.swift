//
//  FriendCell.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 04/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        self.backgroundColor = .blue
    }

}

class MessageCell: BaseCell {
    
    var message: Message?{
        didSet{
            nameLabel.text = message?.friend?.name
            messageLabel.text = message?.text
            guard let imageProfile = message?.friend?.profileImageName else {return}
            imageProfileView.image = UIImage(named: imageProfile)
            guard let date = message?.date else {return}
            timeLabel.text = date.formatterToString(format: .df_h_m_am_pm, style: .medium)
            
        }
    }
    
    let imageProfileView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode  = .scaleAspectFill
        imageView.layer.cornerRadius = 26
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let deviderLine: UIView = {
       let line = UIView()
        line.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        return line
    }()
    
    let nameLabel: UILabel = {
       let label = UILabel()
        label.text = "gandhi mena"
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.text = "Message Label usdhovadiuvoiavoih iovhdoivh ioadv oiadh"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    let timeLabel: UILabel = {
       let label = UILabel()
        label.text = "12:40PM"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let rightArrowImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        //imageView.backgroundColor = .green
        //imageView.layer.cornerRadius = 34
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override func setupViews(){
        //self.backgroundColor = .blue
        addSubview(imageProfileView)
        addSubview(deviderLine)
        
        //addTime
        //addSubview(timeLabel)
        
        setupContainerView()
        
        imageProfileView.image = UIImage(named: "gandhi")
        rightArrowImage.image = UIImage(named: "right-arrow")
        
        //MARK: - Constrains ImageProfile
        addConstrainWithFormat(format: "H:|-12-[v0(\(imageProfileView.layer.cornerRadius * 2))]", views: imageProfileView)
        addConstrainWithFormat(format: "V:|-12-[v0(\(imageProfileView.layer.cornerRadius * 2))]", views: imageProfileView)
        
        addConstraint(NSLayoutConstraint(item: imageProfileView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        //MARK: - Constrains deviderLine
        addConstrainWithFormat(format: "H:|-82-[v0]|", views: deviderLine)
        addConstrainWithFormat(format: "V:[v0(1)]|", views: deviderLine)
        
        //MARK: - Constrains timeLabel
//        addConstrainWithFormat(format: "H:[v0]-10-|", views: timeLabel)
//        addConstrainWithFormat(format: "V:|-12-[v0]", views: timeLabel)
        
    }
    
    func setupContainerView(){
        
        //Create container info
        let containerView = UIView()
        //containerView.backgroundColor = .red
        addSubview(containerView)
        
        //containerInfo Constrains
        addConstrainWithFormat(format: "H:|-80-[v0]|", views: containerView)
        addConstrainWithFormat(format: "V:[v0(50)]", views: containerView)
        addConstraint(NSLayoutConstraint(item: containerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        //add Elements to Container
        containerView.addSubview(nameLabel)
        containerView.addSubview(messageLabel)
        containerView.addSubview(rightArrowImage)
        
        
        containerView.addConstrainWithFormat(format: "H:|[v0]|", views: nameLabel)
        //withMessage
        //containerView.addConstrainWithFormat(format: "V:|[v0][v1(24)]|", views: nameLabel, messageLabel)
        //containerView.addConstrainWithFormat(format: "H:|[v0]-8-[v1(20)]-10-|", views: messageLabel, rightArrowImage)
        
        //withOutMessage
        containerView.addConstrainWithFormat(format: "V:|[v0]|", views: nameLabel)
        containerView.addConstrainWithFormat(format: "H:|[v0]-8-[v1(20)]-10-|", views: nameLabel, rightArrowImage)
        
        containerView.addConstrainWithFormat(format: "V:|-15-[v0(20)]-10-|", views: rightArrowImage)
        
    }
}





























