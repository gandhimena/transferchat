//
//  String+Extension.swift
//  FacebookChat
//
//  Created by Gandhi Mena Salas on 10/05/18.
//  Copyright © 2018 Gandhi Mena Salas. All rights reserved.
//

import Foundation

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
